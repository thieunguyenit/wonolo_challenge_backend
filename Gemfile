source 'https://rubygems.org'

# framework
gem 'rails', '4.2.10'

# constants
gem 'easy_settings', '0.1.2'

# modeling
gem 'virtus', '1.0.5'

# http
gem 'httparty', '0.13.7'

# pagination
gem 'kaminari', '0.16.3'

# rack
gem 'rack-cors', '~> 0.4.0'

group :development do
  # application preloader
  gem 'spring', '2.0.2'
  gem 'spring-watcher-listen', '~> 2.0'
end

group :test do
  # data
  gem 'factory_bot_rails', '4.8.2'
  gem 'faker', '1.8.7'

  # rspec
  gem 'rspec-rails', '3.7.2'
  gem 'json_spec', '1.1.4'
end

group :test, :development do
  # database
  gem 'sqlite3', '1.3.13'

  # constants
  gem 'dotenv-rails', '2.2.1'

  # debug
  gem 'pry-byebug', '3.6.0'
end

group :production do
  gem 'pg', '~> 0.20'
  gem 'rails_12factor'
end