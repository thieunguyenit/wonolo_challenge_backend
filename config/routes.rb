Rails.application.routes.draw do
  namespace :api, { format: 'json' } do
    api_path = EasySettings.api_path

    namespace :v1 do
      resources :search, controller: "#{api_path}/search", only: [:create]
    end
  end
end
