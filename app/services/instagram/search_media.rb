module Services
  module Instagram
    class SearchMedia
      include Services::Base

      def initialize(form, api_client)
        @lat        = form.lat
        @lng        = form.lng
        @distance   = form.radius * 1000
        @api_client = api_client
      end

      def call
        begin
          response = @api_client.search(query: {lat: @lat, lng: @lng, distance: @distance})
        rescue StandardError => err
          success = false
        else
          success = true
        end
        OpenStruct.new(success?: success, data: response)
      end
    end
  end
end