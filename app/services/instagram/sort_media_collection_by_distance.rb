module Services
  module Instagram
    class SortMediaCollectionByDistance
      include Services::Base

      def initialize(collection = [])
        @collection = collection
      end

      def call
        # Sort by distance, closest first
        @collection.sort do |a, b|
          if a['distance']['value'].blank? then 1
          elsif b['distance']['value'].blank? then -1
          else a['distance']['value'] <=> b['distance']['value'] end
        end
      end
    end
  end
end