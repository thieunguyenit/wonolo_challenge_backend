module Services
  module Instagram
    class PopulateMediumData
      include Services::Base

      def initialize(attributes = {}, search_form)
        @medium_attributes   = attributes
        @from_lat, @form_lng = search_form.lat, search_form.lng
        @to_lat, @to_lng     = (lc = attributes['location']).present? ? [lc['latitude'], lc['longitude']] : []
      end

      def call
        distance_value = Haversine.calculate_distance(@from_lat, @form_lng, @to_lat, @to_lng)
        distance_hash  =  {
                            'value' => distance_value.round(EasySettings.round_size),
                            'unit'  => EasySettings.instagram.distance_unit
                          }
        @medium_attributes.merge!({'distance' => distance_hash})
      end
    end
  end
end