module Services
  module Base
    module ClassMethods
      def call(*args)
        new(*args).call
      end
    end

    def self.included(receiver)
      receiver.extend ClassMethods
    end
  end
end