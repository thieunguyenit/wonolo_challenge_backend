module Services
  class PaginateArrayList
    include Services::Base

    def initialize(list = [], options = {})
      @list     = list
      @page     = options[:page] || EasySettings.pagination.page
      @per_page = options[:per_page] || EasySettings.pagination.per
    end

    def call
      total_count    = @list.size
      paginated_data = Kaminari.paginate_array(@list, total_count: total_count).page(@page).per(@per_page)
      {
        data: paginated_data,
        meta: Virtual::Pagination.new(paginated_data)
      }
    end
  end
end