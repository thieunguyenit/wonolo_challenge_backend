module Virtual
  class Pagination
    include ActiveModel::Model

    attr_reader :total_count,
                :limit_value,
                :current_page,
                :next_page,
                :total_pages,
                :is_first_page,
                :is_last_page

    def initialize(object)
      @total_count   = object.total_count
      @limit_value   = object.limit_value
      @current_page  = object.current_page
      @next_page     = object.next_page
      @total_pages   = object.total_pages
      @is_first_page = object.first_page?
      @is_last_page  = object.last_page?
    end
  end
end