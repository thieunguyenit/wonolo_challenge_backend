class Api::V1::BaseApiController < ApplicationController
  def render_error_response(http_status_code, messages = [])
    render  json: {
                    status: Rack::Utils::HTTP_STATUS_CODES[http_status_code],
                    errors: messages
                  },
            status: http_status_code
  end
end