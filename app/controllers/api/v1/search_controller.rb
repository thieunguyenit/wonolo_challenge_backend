module Api
  module V1
    class SearchController < BaseApiController
      def create
        search_form = SearchForm.new(search_params)
        if search_form.invalid?
          render_error_response(400, search_form.errors.full_messages) and return
        end

        instagram_response = Services::Instagram::SearchMedia.call(search_form, InstagramApi::Media.new)
        unless instagram_response.success?
          render_error_response(503, [I18n.t('errors.messages.something_went_wrong')]) and return
        end

        # Populating each medium data and return a new collection
        media = instagram_response.data
        media_with_distance = media.each do |medium|
          Services::Instagram::PopulateMediumData.call(medium, search_form)
        end

        # Sort by distance on the new populated collection
        media_sorted = Services::Instagram::SortMediaCollectionByDistance.call(media_with_distance)

        # Paginating list media (array)
        pagination_options =  {
                                page: params[:page],
                                per_page: EasySettings.pagination.per
                              }
        result = Services::PaginateArrayList.call(media_sorted, pagination_options)
        render json: result
      end

      private

      def search_params
        params.require(:search).permit(:lat, :lng, :radius)
      end
    end
  end
end