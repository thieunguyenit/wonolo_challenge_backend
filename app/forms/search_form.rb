class SearchForm < FormBase
  attribute :lat, Float
  attribute :lng, Float
  attribute :radius, Integer

  validates :lat, presence: true,
                  numericality:  {
                                    greater_than_or_equal_to: EasySettings.instagram.min_lattitude,
                                    less_than_or_equal_to: EasySettings.instagram.max_lattitude
                                  }

  validates :lng, presence: true,
                  numericality:  {
                                    greater_than_or_equal_to: EasySettings.instagram.min_longitude,
                                    less_than_or_equal_to: EasySettings.instagram.max_longitude
                                  }

  validates :radius, presence: true,
                     numericality:  {
                                      only_integer: true,
                                      greater_than_or_equal_to: EasySettings.instagram.min_radius,
                                      less_than_or_equal_to: EasySettings.instagram.max_radius
                                    }
end