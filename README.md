## Requirements
- Ruby 2.x
- Rails 4.x

## Setup
``` bash
# clone project to your local
git clone git@bitbucket.org:thieunguyenit/wonolo_challenge_backend.git
cd wonolo_challenge_backend

# install gems
bundle install

# start server
rails s
```
*Notes*: Remember to create .env file in the root of your project and add:
``` bash
INSTAGRAM_ACCESS_TOKEN=your-token
```

## RSpec
``` bash
RAILS_ENV=test bundle exec rspec spec/
```