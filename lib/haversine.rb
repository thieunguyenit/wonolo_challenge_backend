module Haversine
  extend self

  # Radius of the Earth, in kilometers.
  # Value taken from: http://en.wikipedia.org/wiki/Earth_radius
  EARTH_RADIUS = 6371.0

  # Calculate distance between two points on Earth
  # Using Haversine formula
  # Input:
  #   + lat1 & lng1: Coordinates of point A
  #   + lat2 & lng2: Coordinates of point B
  # Return output in unit: km (kilometre)
  #
  # References:
  #  https://github.com/alexreisner/geocoder/blob/b5af9506a3462b57af0cecef597d2569e204c967/lib/geocoder/calculations.rb
  #  https://en.wikipedia.org/wiki/Haversine_formula
  def calculate_distance(lat1, lng1, lat2, lng2)
    return nil if lat1.blank? || lng1.blank? || lat2.blank? || lng2.blank?

    # convert degrees to radians
    rlat1, rlng1, rlat2, rlng2 = [lat1, lng1, lat2, lng2].map { |d| as_radians(d)}

    # compute deltas
    dlng = rlng1 - rlng2
    dlat = rlat1 - rlat2

    a = power(Math::sin(dlat/2), 2) + Math::cos(rlat1) * Math::cos(rlat2) * power(Math::sin(dlng/2), 2)
    great_circle_distance = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1-a))
    EARTH_RADIUS * great_circle_distance
  end

  # Convert degrees to radians
  def as_radians(degrees)
    degrees * (Math::PI / 180)
  end

  # Return the value of the number 'num' to the power of 'pow'
  def power(num, pow)
    num ** pow
  end
end