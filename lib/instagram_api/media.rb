module InstagramApi
  class Media
    include HTTParty
    base_uri EasySettings.instagram.base_url

    def initialize
      self.class.default_params access_token: ENV['INSTAGRAM_ACCESS_TOKEN']
    end

    def search(options = {})
      response = self.class.get('/media/search', options)['data']
      response || []
    end

    def get_media(id)
      self.class.get("/media/#{id}")['data']
    end
  end
end