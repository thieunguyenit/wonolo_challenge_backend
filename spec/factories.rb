FactoryBot.define do
  sequence(:latitude) { Faker::Number.rand(-90.000000000...90.000000000) }
  sequence(:longitude) { Faker::Number.rand(-180.000000000...180.000000000) }
  sequence(:number_2) { Faker::Number.number(2) }
  sequence(:number_5) { Faker::Number.number(5) }
  sequence(:number_10) { Faker::Number.number(10) }
  sequence(:number_20) { Faker::Number.number(20) }
  sequence(:hexadecimal_10) { Faker::Number.hexadecimal(10) }
  sequence(:name) { Faker::Name.name }
  sequence(:url) { Faker::Internet.url }
  sequence(:image) { Faker::Avatar.image }
  sequence(:sentence) { Faker::Lorem.sentence }
  sequence(:address) { Faker::Address.street_address }
  sequence(:radius) { Faker::Number.rand(EasySettings.instagram.min_radius..EasySettings.instagram.max_radius) }
end