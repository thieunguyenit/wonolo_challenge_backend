require 'rails_helper'

RSpec.describe Haversine do

  describe '#calculate_distance' do
    context 'When latitude or longtitude parameter is empty' do
      it 'Return distance is nil' do
        # execute
        actual = described_class.calculate_distance(nil, 0, 0, 1)

        # verify
        expect(actual).to be_nil
      end
    end

    context 'When all parameter have value' do
      it 'Return distance is float number' do
        # execute
        actual = described_class.calculate_distance(0, 0, 0, 1)

        # verify
        expect(actual).not_to be_nil
        expect(actual).to be_an_instance_of(Float)
        expect(actual.round).to eql(111)
      end
    end
  end
end