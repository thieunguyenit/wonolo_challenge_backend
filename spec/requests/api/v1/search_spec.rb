require 'rails_helper'

RSpec.describe 'Api::V1::Search', type: :request do

  let(:search_params) do
    params = {}
    params[:search] = {
      lat: generate(:latitude),
      lng: generate(:longitude),
      radius: generate(:radius)
    }
    params[:page] = 1
    params
  end

  let(:list_instagram_media) do
    (1..20).map do |i|
      medium = build(:instagram_image).attributes
    end
  end

  describe 'POST /api/v1/search' do
    context 'System works correctly' do
      context 'When connect to instagram api successful' do
        it 'Return instagram media list and pagination information' do
          # setup
          allow(Services::Instagram::SearchMedia)
            .to receive(:call)
            .and_return(OpenStruct.new(success?: true, data: list_instagram_media))

          # execute
          post '/api/v1/search', search_params.merge({page: 2, per_page: 6})
          actual = response.body

          # verify
          expect(response.status).to eql(200)
          expect(actual).to have_json_path('data')
          expect(actual).to have_json_path('meta')
          expect(response_body_as_json['meta']['limit_value']).to be(6)
          expect(response_body_as_json['meta']['current_page']).to be(2)
        end
      end
    end

    context 'System works incorrectly' do
      context 'When latitude is invalid' do
        context 'When latitude is empty' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:lat] = nil

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lat'), I18n.t('errors.messages.blank')))
            expect(response_body_as_json['errors'][1]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lat'), I18n.t('errors.messages.not_a_number')))
          end
        end

        context 'When latitude value is not a number' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:lat] = "Invalid latitude"

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lat'), I18n.t('errors.messages.not_a_number')))
          end
        end

        context 'When latitude value is greater than the maximum (90)' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:lat] = EasySettings.instagram.max_lattitude + 1

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lat'), I18n.t('errors.messages.less_than_or_equal_to', count: EasySettings.instagram.max_lattitude)))
          end
        end

        context 'When latitude value is less than the minimum (-90)' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:lat] = EasySettings.instagram.min_lattitude - 1

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lat'), I18n.t('errors.messages.greater_than_or_equal_to', count: EasySettings.instagram.min_lattitude)))
          end
        end
      end

      context 'When longitude is invalid' do
        context 'When longitude is empty' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:lng] = nil

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lng'), I18n.t('errors.messages.blank')))
            expect(response_body_as_json['errors'][1]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lng'), I18n.t('errors.messages.not_a_number')))
          end
        end

        context 'When longitude value is not a number' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:lng] = "Invalid longitude"

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lng'), I18n.t('errors.messages.not_a_number')))
          end
        end

        context 'When longitude value is greater than the maximum (180)' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:lng] = EasySettings.instagram.max_longitude + 1

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lng'), I18n.t('errors.messages.less_than_or_equal_to', count: EasySettings.instagram.max_longitude)))
          end
        end

        context 'When longitude value is less than the minimum (-180)' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:lng] = EasySettings.instagram.min_longitude - 1

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.lng'), I18n.t('errors.messages.greater_than_or_equal_to', count: EasySettings.instagram.min_longitude)))
          end
        end
      end

      context 'When radius is invalid' do
        context 'When radius is empty' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:radius] = nil

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.radius'), I18n.t('errors.messages.blank')))
            expect(response_body_as_json['errors'][1]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.radius'), I18n.t('errors.messages.not_a_number')))
          end
        end

        context 'When radius value is not a number' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:radius] = "Invalid radius"

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.radius'), I18n.t('errors.messages.not_a_number')))
          end
        end

        context 'When radius value is greater than the maximum' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:radius] = EasySettings.instagram.max_radius + 1

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.radius'), I18n.t('errors.messages.less_than_or_equal_to', count: EasySettings.instagram.max_radius)))
          end
        end

        context 'When radius value is less than the minimum' do
          it 'Return http status code 400 with error messages' do
            # setup
            search_params[:search][:radius] = EasySettings.instagram.min_radius - 1

            # execute
            post '/api/v1/search', search_params

            # verify
            expect(response.status).to eql(400)
            expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[400])
            expect(response_body_as_json['errors'][0]).to eql(get_error_message(I18n.t('activemodel.attributes.search_form.radius'), I18n.t('errors.messages.greater_than_or_equal_to', count: EasySettings.instagram.min_radius)))
          end
        end
      end

      context 'When connect to instagram api unsuccessful' do
        it 'Return http status code 503 with error messages' do
          # setup
          allow(Services::Instagram::SearchMedia)
            .to receive(:call)
            .and_return(OpenStruct.new(success?: false, data: nil))

          # execute
          post '/api/v1/search', search_params

          # verify
          expect(response.status).to eql(503)
          expect(response_body_as_json['status']).to eql(Rack::Utils::HTTP_STATUS_CODES[503])
          expect(response_body_as_json['errors'][0]).to eql(I18n.t('errors.messages.something_went_wrong'))
        end
      end
    end
  end
end