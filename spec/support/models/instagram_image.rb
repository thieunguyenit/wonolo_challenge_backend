class InstagramImage

  attr_accessor :id,
                :user,
                :images,
                :created_time,
                :caption,
                :user_has_liked,
                :likes,
                :tags,
                :filter,
                :comments,
                :type,
                :link,
                :location,
                :attribution,
                :users_in_photo

  def attributes
    {
      'id'             => @id,
      'user'           => @user,
      'images'         => @images,
      'created_time'   => @created_time,
      'caption'        => @caption,
      'user_has_liked' => @user_has_liked,
      'likes'          => @likes,
      'tags'           => @tags,
      'filter'         => @filter,
      'comments'       => @comments,
      'type'           => @type,
      'link'           => @link,
      'location'       => @location,
      'attribution'    => @attribution,
      'users_in_photo' => @users_in_photo
    }
  end
end