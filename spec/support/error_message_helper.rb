module ErrorMessageHelper
  def get_error_message(attribute, message)
    I18n.t('errors.format', attribute: attribute, message: message)
  end
end

RSpec.configure do |config|
  config.include ErrorMessageHelper, type: :request
end