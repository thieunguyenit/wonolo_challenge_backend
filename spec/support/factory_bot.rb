# Include all strategy methods provided by FactoryBot
RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end