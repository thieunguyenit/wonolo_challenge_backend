module RequestJsonHelper
  def response_body_as_json
    JSON.parse(response.body)
  end
end

module ErrorMessageHelper
  def get_error_message(attribute, message)
    I18n.t('errors.format', attribute: attribute, message: message)
  end
end

# DRY the command JSON.parse(response.body) which is used a lot in our tests
RSpec.configure do |config|
  config.include RequestJsonHelper, type: :request
end