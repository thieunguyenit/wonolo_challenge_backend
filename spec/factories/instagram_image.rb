user = {
  'id'              => FactoryBot.generate(:number_10),
  'full_name'       => FactoryBot.generate(:name),
  'profile_picture' => FactoryBot.generate(:image),
  'username'        => 'username'
}

images = {
  'thumbnail' => {
    'width'  => 150,
    'height' => 150,
    'url'    => FactoryBot.generate(:image)
  },
  'low_resolution' => {
    'width'  => 320,
    'height' => 320,
    'url'    => FactoryBot.generate(:image)
  },
  'standard_resolution' => {
    'width'  => 640,
    'height' => 640,
    'url'    => FactoryBot.generate(:image)
  }
}

likes = {
  'count' => FactoryBot.generate(:number_2)
}

comments = {
  'count' => FactoryBot.generate(:number_2)
}

location = {
  'latitude'  => FactoryBot.generate(:latitude),
  'longitude' => FactoryBot.generate(:longitude),
  'name'      => FactoryBot.generate(:address),
  'id'        => FactoryBot.generate(:number_5)
}

FactoryBot.define do
  factory :instagram_image do
    id { generate :number_20 }
    user { user }
    images { images }
    created_time { generate :number_10 }
    caption { generate :sentence }
    user_has_liked { Faker::Boolean.boolean }
    likes { likes }
    tags []
    filter 'Normal'
    comments { comments }
    type 'image'
    link { generate :url }
    location { location }
    attribution nil
    users_in_photo []
  end
end