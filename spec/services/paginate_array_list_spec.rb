require 'rails_helper'

RSpec.describe Services::PaginateArrayList do

  let(:array_list) { (1..30).to_a }

  describe '.call' do
    it 'return object (hash type) which also contains paging metadata' do
      # setup
      page     = 1
      per_page = 10

      # execute
      actual = described_class.call(array_list, { page: page, per_page: per_page })

      # verify
      expect(actual).to be_an_instance_of(Hash)
      expect(actual).to have_key(:data)
      expect(actual).to have_key(:meta)
      expect(actual[:data].size).to eql(per_page)
    end
  end
end