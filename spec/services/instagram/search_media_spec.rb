require 'rails_helper'

RSpec.describe Services::Instagram::SearchMedia do

  let(:search_form) { SearchForm.new(lat: generate(:latitude), lng: generate(:longitude), radius: generate(:radius)) }
  let(:list_instagram_media) do
    (1..5).map do |i|
      medium = build(:instagram_image).attributes
    end
  end

  describe '.call' do
    context 'When call external api successful' do
      it 'Return a OpenStruct object contains list instagram media with response status is true' do
        # setup
        api_client = InstagramApi::Media.new
        allow(api_client).to receive(:search).and_return(list_instagram_media)

        # execute
        actual = described_class.call(search_form, api_client)

        # verify
        expect(actual).to be_an_instance_of(OpenStruct)
        expect(actual.data).to eql(list_instagram_media)
        expect(actual.success?).to eql(true)
      end
    end

    context 'When call external api raise an exception' do
      it 'Return a OpenStruct with response status is false' do
        # setup
        api_client = InstagramApi::Media.new
        allow(api_client).to receive(:search).and_raise(StandardError.new('error'))

        # execute
        actual = described_class.call(search_form, api_client)

        # verify
        expect(actual).to be_an_instance_of(OpenStruct)
        expect(actual.data).to be_nil
        expect(actual.success?).to eql(false)
      end
    end
  end
end