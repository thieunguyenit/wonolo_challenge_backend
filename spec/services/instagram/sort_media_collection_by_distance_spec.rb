require 'rails_helper'

RSpec.describe Services::Instagram::SortMediaCollectionByDistance do

  let(:list_instagram_media) do
    (1..5).map do |i|
      medium = build(:instagram_image).attributes
      # Add distance information
      distance = {
        'value' => i,
        'unit' => EasySettings.instagram.distance_unit
      }
      medium.merge!({'distance' => distance})
    end
  end

  describe '.call' do
    context 'When a medium has distance value is nil' do
      it 'Return the sorted list with medium (distance nil) is at the end of list' do
        # setup
        list_instagram_media[0]['distance']['value'] = nil

        # execute
        actual = described_class.call(list_instagram_media)

        # verify
        expect(actual[0]).to eql(list_instagram_media[1])
        expect(actual[4]).to eql(list_instagram_media[0])
      end
    end

    context 'When all media in list have distance value not nil' do
      it 'Return the sorted list with distance increasingly' do
        # execute
        actual = described_class.call(list_instagram_media)

        # verify
        for i in 1..5 do
          expect(actual[i-1]).to eql(list_instagram_media[i-1])
        end
      end
    end
  end
end