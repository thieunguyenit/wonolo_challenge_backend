require 'rails_helper'

RSpec.describe Services::Instagram::PopulateMediumData do

  let(:instagram_medium) { build(:instagram_image) }
  let(:search_form) { SearchForm.new(lat: generate(:latitude), lng: generate(:longitude), radius: generate(:radius)) }

  describe '.call' do
    it "Return instagram medium's attributes including distance info (hash type)" do
      # setup
      medium_attributes = instagram_medium.attributes

      # execute
      actual = described_class.call(medium_attributes, search_form)

      # verify
      expect(actual).to be_an_instance_of(Hash)
      expect(actual).to have_key('distance')
      expect(actual.dig('distance')).to be_an_instance_of(Hash)
    end
  end
end